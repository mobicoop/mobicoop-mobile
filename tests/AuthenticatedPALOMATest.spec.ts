import { expect } from '@playwright/test';
import { test } from '../fixtures.js';
import { Utils } from './utils';

const EMAIL = process.env.EMAILPALOMA;
const PASSWORD = process.env.PASSWORDPALOMA;
const EMAILLABEL = 'EMAILPALOMA';
const PASSWORDLABEL = 'PASSWORDPALOMA';

test.beforeEach(async ({ page }) => {

    await Utils.connect(EMAIL, PASSWORD, EMAILLABEL, PASSWORDLABEL, page, test);

});

test.afterEach(async ({ page }) => {

    await Utils.disconnect(EMAIL, PASSWORD, EMAILLABEL, PASSWORDLABEL, page, test, expect);

});

test('RC1.1 - Publier une annonce ponctuelle', async ({ page }) => {

    test.skip(process.env.POST !== 'on', 'need POST=on to enable this test');

    await page.locator('#originButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=origin&action=search');

    await page.locator('input[name="search"]').fill('5 rue de la Monnaie Nancy');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    // wait for 1 second
    await page.waitForTimeout(1000);

    await page.locator('#destinationButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=destination&action=search');

    await page.locator('input[name="search"]').fill('9 bd Louis Sicre Castelsarrasin');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    await page.waitForTimeout(1000);

    await page.locator('#postCarpoolButton').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool');

    await page.locator('.mc-select-type >> button >> nth=-1').click();

    await page.locator('ion-datetime >> button').click();

    await page.locator('.picker-toolbar-button >> nth=-1').click();

    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step');

    await page.locator('ion-datetime >> nth=2 >> button').click();

    await page.locator('.picker-toolbar-button >> nth=-1').click();

    await page.locator('ion-checkbox >> button').click();


    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step?step=1');

    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step?step=2');

    await page.locator('#seatsInput >> input >> nth=-1').fill('2');

    await page.locator('#backSeatsButton >> button').click();

    await page.locator('ion-button >> nth=-1').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step?step=3');

    await page.locator('.mc-carpool-price-input >> input >> nth=-1').fill('80');

    await expect(page.locator('.mc-carpool-price-input >> span')).toHaveText('0.10€/km');

    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step?step=4');

    await page.locator('.mc-form-carpool-message >> textarea >> nth=-1').fill('Sans commentaire, je suis un robot.');

    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/post-carpool-step?step=5');

    await page.locator('ion-button >> nth=-1').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/my-carpools');

    await page.locator('ion-header >> button').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/profile');

});

async function setTime(page, targetTime, nth) {
    while (true) {
        await page.waitForTimeout(150);

        let currentStartTime = await page.locator('.picker-opt-selected >> nth=' + nth).textContent();
        const compare = currentStartTime.localeCompare(targetTime);
        if (compare == 0) {
            break;
        } else if (compare > 0) {
            await page.locator('button:above(:nth-match(.picker-opt-selected,' + (nth + 1) + '))').first().click();
        } else {
            await page.locator('button:below(:nth-match(.picker-opt-selected,' + (nth + 1) + '))').first().click();
        }
    }

}
