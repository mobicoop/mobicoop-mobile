import { expect } from '@playwright/test';
import { test } from '../fixtures.js';

test.beforeEach(async ({ page }) => {

    await page.goto(process.env.URL);

    await page.locator('[id="cookieButton"]').click();

});

test('Rechercher un covoiturage ponctuel', async ({ page }) => {

    await page.locator('#originButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=origin&action=search');

    await page.locator('input[name="search"]').fill('Nancy, 54');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    // wait for 1 second
    await page.waitForTimeout(1000);

    await page.locator('#destinationButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=destination&action=search');

    await page.locator('input[name="search"]').fill('Metz, 57');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    await page.waitForTimeout(1000);

    await page.locator('#datePicker').click();

    await page.locator('.picker-toolbar-button >> nth=-1').click();

    await page.waitForTimeout(1000);

    await page.locator('#searchButton').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/search');

    const firstResponse = page.locator('.mc-carpool-item >> nth=0');

    const d = new Date();

    const dateLocator = d.toLocaleDateString('fr-FR').slice(0, 5).replace('/', '.');

    await expect(firstResponse.locator('span', { hasText: dateLocator })).toBeVisible();

    await expect(firstResponse.locator('text=3,5 €')).toBeVisible();

    await expect(firstResponse.locator('text=08:02')).toBeVisible();

    await expect(firstResponse.locator('.mc-carpool-carpooler')).toHaveText(/Colin P./);

    await page.locator('ion-header[role="banner"] button').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

});

test('Rechercher un covoiturage régulier', async ({ page }) => {

    await page.locator('#originButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=origin&action=search');

    await page.locator('input[name="search"]').fill('Houdemont, 54');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    // wait for 1 second
    await page.waitForTimeout(1000);

    await page.locator('#destinationButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=destination&action=search');

    await page.locator('input[name="search"]').fill('Seichamps, 54');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    await page.locator('ion-checkbox').click();

    //    await page.waitForTimeout(1000);

    await page.locator('#searchButton').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/search');

    const firstResponse = page.locator('.mc-carpool-item >> nth=0');

    const d = new Date();

    const dateLocator = d.toLocaleDateString('fr-FR').slice(0, 5).replace('/', '.');

    await expect(firstResponse.locator('span', { hasText: dateLocator })).toBeVisible();

    await expect(firstResponse.locator('text=0.7 €')).toBeVisible();

    await expect(firstResponse.locator('text=07/40')).toBeVisible();

    await expect(firstResponse.locator('.mc-carpool-carpooler')).toHaveText(/Cora R./);

    await page.locator('ion-header[role="banner"] button').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

});