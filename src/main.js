/**

 Copyright (c) 2018, MOBICOOP. All rights reserved.
 This project is dual licensed under AGPL and proprietary licence.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see gnu.org/licenses.

 Licence MOBICOOP described in the file
 LICENSE
 **************************/

import {createApp} from 'vue'
import App from './App.vue'
import router from './router';
import i18n from './i18n';
import store from './Shared/Store/store';
import * as Sentry from "@sentry/vue";
import 'leaflet/dist/leaflet.css';
import 'leaflet-fullscreen/dist/leaflet.fullscreen.css';
// import 'leaflet-fullscreen/dist/Leaflet.fullscreen';
import {
  IonContent,
  IonPage,
  IonicVue,
  IonTabs,
  IonTabBar,
  IonTabButton,
  IonIcon,
  IonRouterOutlet,
  IonButton,
  IonText,
  IonLabel,
  IonBadge,
  IonList,
  IonItem,
  IonItemSliding,
  IonAvatar,
  IonSegment,
  IonSegmentButton,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonGrid,
  IonRow,
  IonCheckbox,
  IonCard,
  IonCardContent,
  IonCol,
  IonThumbnail,
  IonInput,
  IonDatetime,
  IonDatetimeButton,
  IonSearchbar,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonModal,
  IonCardTitle,
  IonCardHeader,
  IonSlides,
  IonSlide,
  IonCardSubtitle,
  IonSelect,
  IonSelectOption,
  IonSpinner,
  IonRadio, IonRadioGroup, IonToggle, IonTextarea, IonFabButton, IonProgressBar, IonFab, IonTitle, IonRange, IonChip, IonBackdrop
} from '@ionic/vue';
import '@ionic/core/css/ionic.bundle.css';
const moment = require('moment')
import { addIcons } from 'ionicons';
import * as allIcons from 'ionicons/icons';
require('moment/locale/fr')
import { StatusBar, Style } from '@capacitor/status-bar';
import {Swiper, SwiperSlide} from "swiper/vue";

/*
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
   iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
   iconUrl: require('leaflet/dist/images/marker-icon.png'),
   shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
*/



const currentIcons = Object.keys(allIcons).map(i => {
  const key = i.replace(/[A-Z]/g, letter => `-${letter.toLowerCase()}`)
  if(typeof allIcons[i] === 'string') {
    return {
      [key]: allIcons[i],
    }
  }
  return {
    ['ios-' + key]: allIcons[i].ios,
    ['md-' + key]: allIcons[i].md,
  };
});

const iconsObject = Object.assign({}, ...currentIcons);
addIcons(iconsObject);


/*
new Vue({
  router,
  i18n,
  el: '#app',
  store,
  render: h => h(App),
})

 */

const app = createApp(App);

// Default locale
i18n.locale = 'fr';
i18n.test = 'a'

app.use(store); // Utilisez Vuex dans l'application
app.use(router);
app.use(i18n);
app.use(IonicVue, { 'backButtonText' : '', innerHTMLTemplatesEnabled: true });
app.config.productionTip = false;
app.config.globalProperties.$moment = moment;
//app.config.globalProperties.$router = router;
const momentFormat = (date, format, utc) => {
  return utc ? moment(date).utc().format(format) : moment(date).format(format) ;
};
app.config.globalProperties.moment = momentFormat;

app.component('ion-title', IonTitle);
app.component('ion-content', IonContent);
app.component('ion-page', IonPage);
app.component('ion-router-outlet', IonRouterOutlet);
app.component('ion-tabs', IonTabs);
app.component('ion-tab-bar', IonTabBar);
app.component('ion-tab-button', IonTabButton);
app.component('ion-icon', IonIcon);
app.component('ion-button', IonButton);
app.component('ion-text', IonText);
app.component('ion-badge', IonBadge);
app.component('ion-label', IonLabel);
app.component('ion-list', IonList);
app.component('ion-item', IonItem);
app.component('ion-item-sliding', IonItemSliding);
app.component('ion-avatar', IonAvatar);
app.component('ion-segment', IonSegment);
app.component('ion-segment-button', IonSegmentButton);
app.component('ion-header', IonHeader);
app.component('ion-toolbar', IonToolbar);
app.component('ion-buttons', IonButtons);
app.component('ion-back-button', IonBackButton);
app.component('ion-grid', IonGrid);
app.component('ion-row', IonRow);
app.component('ion-col', IonCol);
app.component('ion-checkbox', IonCheckbox);
app.component('ion-card', IonCard);
app.component('ion-card-title', IonCardTitle);
app.component('ion-card-subtitle', IonCardSubtitle);
app.component('ion-card-header', IonCardHeader);
app.component('ion-card-content', IonCardContent);
app.component('ion-thumbnail', IonThumbnail);
app.component('ion-input', IonInput);
app.component('ion-datetime', IonDatetime);
app.component('ion-datetime-button', IonDatetimeButton);
app.component('ion-searchbar', IonSearchbar);
app.component('ion-infinite-scroll', IonInfiniteScroll);
app.component('ion-infinite-scroll-content', IonInfiniteScrollContent);
app.component('ion-modal', IonModal);
app.component('ion-select', IonSelect);
app.component('ion-select-option', IonSelectOption);
app.component('ion-spinner', IonSpinner);
app.component('ion-radio', IonRadio);
app.component('ion-radio-group', IonRadioGroup);
app.component('ion-toggle', IonToggle);
app.component('ion-textarea', IonTextarea);
app.component('ion-fab', IonFab);
app.component('ion-fab-button', IonFabButton);
app.component('ion-progress-bar', IonProgressBar);
app.component('ion-range', IonRange);
app.component('ion-chip', IonChip);
app.component('ion-backdrop', IonBackdrop);
app.component('swiper', Swiper);
app.component('swiper-slide', SwiperSlide);


Sentry.init({
  app,
  dsn: "https://3ffae0816e54405e9b1b5b560c18ec1d@sentry.mobicoop.io/4",
  integrations: [
    new Sentry.BrowserTracing({
      // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
      // tracePropagationTargets: ["localhost", process.env.VUE_APP_API_URL],
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
    }),
    new Sentry.Replay(),
  ],

  initialScope: {
    tags: {"instance": process.env.VUE_APP_INSTANCE}
  },

  trackComponents: true,
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,

  // Capture Replay for 10% of all sessions,
  // plus for 100% of sessions with an error
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
});

app.mount('#app');







