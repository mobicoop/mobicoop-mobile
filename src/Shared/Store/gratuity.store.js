import http from "../Mixin/http.mixin";

/**

Copyright (c) 2018, MOBICOOP. All rights reserved.
This project is dual licensed under AGPL and proprietary licence.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License
along with this program. If not, see gnu.org/licenses.

Licence MOBICOOP described in the file
LICENSE
**************************/

export const gratuityStore = {
  state: {
    statusGratuity: 'loading',
    displayedGratuities: [],
  },
  mutations: {
    add_displayed_gratuities(state, gratuities) {
      if (!!gratuities && Array.isArray(gratuities)) {
        const filteredGratuities = gratuities.filter(item => !state.displayedGratuities.map(b => b.id).includes(item.id))
        state.displayedGratuities.push(...filteredGratuities);
      }
    },
    remove_displayed_gratuities(state, gratuity) {
      const i = state.displayedGratuities.findIndex(item => item.id === gratuity.id);
      if (i > -1) {
        state.displayedGratuities.splice(i, 1);
      }
      return new Promise((resolve, reject) => {
        http.get("/gratuity_campaigns/" + gratuity.id + "/tagAsNotified" ).then(resp => {
          if (resp) {
            resolve(resp)
          }
        }).catch(err => {
          reject(err)
        })
      })
    }
  },
  actions: {

  },
  getters: {
    getDisplayedGratuities: state => {
      return state.displayedGratuities;
    },
  }
}
export default {
  gratuityStore
};
