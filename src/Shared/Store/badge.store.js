import http from "../Mixin/http.mixin";

/**

Copyright (c) 2018, MOBICOOP. All rights reserved.
This project is dual licensed under AGPL and proprietary licence.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License
along with this program. If not, see gnu.org/licenses.

Licence MOBICOOP described in the file
LICENSE
**************************/

export const badgeStore = {
  state: {
    statusBadges: 'loading',
    displayedBadges: [],
    winBadge: null,
  },
  mutations: {
    add_displayed_badges(state, badges) {
      if (!!badges && Array.isArray(badges)) {
        const filteredBadges = badges.filter(item => !state.displayedBadges.map(b => b.id).includes(item.id))
        state.displayedBadges.push(...filteredBadges);
      }
    },
    remove_displayed_badges(state, badge) {
      const i = state.displayedBadges.findIndex(item => item.id === badge.id);
      if (i > -1) {
        state.displayedBadges.splice(i, 1);
      }
      if (badge.type === 'RewardStep') {
        return new Promise((resolve, reject) => {
            http.get("/reward_steps/" + badge.id + "/tagAsNotified" ).then(resp => {
              if (resp) {
                resolve(resp)
              }
            }).catch(err => {
              reject(err)
            })
          })
      } else if (badge.type === 'Badge') {
        return new Promise((resolve, reject) => {
          http.get("/rewards/" + badge.rewardId + "/tagAsNotified" ).then(resp => {
            if (resp) {
              resolve(resp)
            }
          }).catch(err => {
            reject(err)
          })
        })
      }
    },
    set_win_badge(state, w) {
      state.winBadge = w;
    },
  },
  actions: {
    getMyBadges({ commit }) {
      return new Promise((resolve, reject) => {
        http.get("/badges_boards" ).then(resp => {
          if (resp) {
            resolve(resp)
          }
        }).catch(err => {
          reject(err)
        })
      })
    },
    getAllBadges({ commit }) {
      return new Promise((resolve, reject) => {
        http.get("/badges" ).then(resp => {
          if (resp) {
            resolve(resp)
          }
        }).catch(err => {
          reject(err)
        })
      })
    },
    getBadge({ commit }, id) {
      return new Promise((resolve, reject) => {
        http.get("/badges/" + id ).then(resp => {
          if (resp) {
            resolve(resp)
          }
        }).catch(err => {
          reject(err)
        })
      })
    },
  },
  getters: {
    getDisplayedBadges: state => {
      return state.displayedBadges;
    },
    getWinBadge: state => {
      return state.winBadge;
    },
  }
}
export default {
  badgeStore
};
