/**

 Copyright (c) 2018, MOBICOOP. All rights reserved.
 This project is dual licensed under AGPL and proprietary licence.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see gnu.org/licenses.

 Licence MOBICOOP described in the file
 LICENSE
 **************************/

import { createStore } from 'vuex';
import { userStore } from './user.store';
import { searchStore } from './search.store';
import { registerStore } from './register.store';
import { carpoolStore } from './carpool.store';
import { messageStore } from "./message.store";
import { sliderStore } from './slider.store';
import { articleStore } from './article.store';
import { communityStore } from './community.store';
import { eventStore } from './event.store';
import { appStore } from './app.store';
import { solidaryTransportStore } from '../../SolidaryTransport/SolidaryTransport.store';
import { hitchHikingStore } from '../../HitchHiking/HitchHiking.store';
import { dynamicStore } from './dynamic.store';
import { publicTransportStore } from './public-transport.store';
import { rdexStore } from './rdex.store';
import { reviewStore } from "./review.store";
import { paymentStore } from './payment.store';
import { proofStore } from './proof.store';
import { badgeStore } from './badge.store';
import createPersistedState from "vuex-persistedstate";
import {relaypointStore} from "./relaypoint.store";
import {ssoStore} from "./sso.store";
import {gratuityStore} from "./gratuity.store";


const modules = {
  userStore: userStore,
  searchStore: searchStore,
  registerStore: registerStore,
  carpoolStore: carpoolStore,
  messageStore: messageStore,
  sliderStore: sliderStore,
  appStore: appStore,
  articleStore: articleStore,
  communityStore: communityStore,
  eventStore: eventStore,
  dynamicStore: dynamicStore,
  publicTransportStore: publicTransportStore,
  rdexStore: rdexStore,
  reviewStore: reviewStore,
  paymentStore: paymentStore,
  proofStore: proofStore,
  badgeStore: badgeStore,
  relaypointStore: relaypointStore,
  ssoStore: ssoStore,
  gratuityStore: gratuityStore
}

let appModule = JSON.parse(process.env.VUE_APP_MODULE);
// Merge locales if Solidarity Transport module enabled
if (appModule.SOLIDARYTRANSPORT) {
  modules['solidaryTransportStore'] = solidaryTransportStore
}

if (appModule.HITCHHIKING) {
  modules['hitchHikingStore'] = hitchHikingStore
}

export default createStore({
  plugins: [createPersistedState()],
  modules
});
