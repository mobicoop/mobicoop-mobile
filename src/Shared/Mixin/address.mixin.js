/**

    Copyright (c) 2018, MOBICOOP. All rights reserved.
    This project is dual licensed under AGPL and proprietary licence.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see gnu.org/licenses.

    Licence MOBICOOP described in the file
    LICENSE
**************************/

export const address = {
  methods: {
    formatAddress(address, noHouseNumber) {
      let displayGeo = '';
      if (address) {
        if (address.displayLabel && address.displayLabel[0]) {
          return Array.isArray(address.displayLabel[0]) ? address.displayLabel[0][0] : address.displayLabel[0];
        }
        if (address.name && address.name !== 'homeAddress') {
          displayGeo += `${address.name}, `;
        }
        if (address.houseNumber && !noHouseNumber) {
          displayGeo += `${address.houseNumber}`;
        }
        if (address.addressLocality) {
          displayGeo += `${address.addressLocality}`;
        }
        if (address.streetAddress) {
          displayGeo += `, ${address.streetAddress}`
        }
        if (address.postalCode) {
          displayGeo += `, ${address.postalCode}`
        }
        if (address.addressCountry) {
          displayGeo += `, ${address.addressCountry}`;
        }
      }
      return displayGeo;
    },
    selectionText(item) {
      let text = '';
      if (item.type == 'street') {
        text += item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'housenumber') {
        text += item.houseNumber + ', ' + item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'venue' || item.type == 'event') {
        text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'user') {
        if (this.showName) text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (this.showName && item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'relaypoint') {
        if (item.name) text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (item.postalCode && item.streetName) text += item.postalCode + ', ';
      }
      if (item.locality) text += item.locality;
      if (item.type == 'locality' && item.regionCode !== null)
        text += ', ' + item.regionCode;
      if (item.type == 'locality' && item.countryCode !== this.country)
        text += ', ' + item.country;
      if (item.type == 'relaypoint' && text == '') {
        text = this.$t('gps') + ' [' + item.lat + ', ' + item.lon + ']';
      }
      if (text.slice(text.length - 2) == ', ') {
        text = text.slice(0, -2);
      }
      return text;
    },
    propositionTitle(item) {
      let text = '';
      if (item.type == 'street') {
        text += item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'housenumber') {
        text += item.houseNumber + ', ' + item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'venue' || item.type == 'event') {
        text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'user') {
        if (this.showName) text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (this.showName && item.postalCode) text += item.postalCode + ', ';
      }
      if (item.type == 'relaypoint') {
        if (item.name) text += item.name + ', ';
        if (item.houseNumber) text += item.houseNumber + ', ';
        if (item.streetName) text += item.streetName + ', ';
        if (item.postalCode && item.streetName) text += item.postalCode + ', ';
      }
      if (item.locality) text += item.locality;
      if (item.type == 'relaypoint' && text == '') {
        text = this.$t('gps') + ' [' + item.lat + ', ' + item.lon + ']';
      }
      if (text.slice(text.length - 2) == ', ') {
        text = text.slice(0, -2);
      }
      return text;
    },
    propositionText(item) {
      let text = '';
      if (item.regionCode) text += item.regionCode;
      if (item.region) text += (text != '' ? ', ' : '') + item.region;
      if (item.macroRegion)
        text += (text != '' ? ', ' : '') + item.macroRegion;
      if (item.country) text += (text != '' ? ', ' : '') + item.country;
      return text;
    },
    propositionTemplate(item) {
      switch(item.type) {
        case "locality" : return { icon: "mdi-city-variant", color: "#536dfe"};
        case "street" : return { icon: "mdi-road-variant", color: "#7c4dff"};
        case "housenumber" : return { icon: "mdi-home-map-marker", color: "#e040fb"};
        case "venue" : return { icon: "mdi-map-marker", color: "#ff4081"};
        case "relaypoint" : return { icon: "mdi-parking", image: item.icon ? item.icon.url : undefined, color: "#000000"};
        case "user" : return { icon: "mdi-home-heart", color: "#64FFDA"};
        case "event" : return { icon: "mdi-stadium-variant", color: "#64FFDA"};
      }
      return { icon: "mdi-earth", color: "#000000"};
    },
    pointToAddress(point) {
      if (point) {
        const address = {
          "displayLabel": point.displayLabel,
          "houseNumber": point.houseNumber,
          "streetAddress": point.streetName,
          "postalCode": point.postalCode,
          "addressLocality":point.locality,
          "region":point.region,
          "regionCode":point.regionCode,
          "macroRegion":point.macroRegion,
          "addressCountry":point.country,
          "countryCode":point.countryCode,
          "latitude":point.lat.toString(),
          "longitude":point.lon.toString(),
          "name":point.name,
          "providedBy":point.provider,
          "distance":point.distance,
          "type":point.type,
          "id":point.id
        };
        if (point.type == "event") {
          // so nice...
          address.event = {"id": point.id, "name": point.name}
        }
        return address;
      }
      return null;
    },
    addressToPoint(address) {
      if (address)
        return  {
          "displayLabel": address.displayLabel,
          "houseNumber":address.houseNumber,
          "streetName":address.street,
          "postalCode":address.postalCode,
          "locality":address.Locality,
          "region":address.region,
          "macroRegion":address.macroRegion,
          "country":address.addressCountry,
          "countryCode":address.countryCode,
          "lat": Number(address.latitude),
          "lon":Number(address.longitude),
          "name":address.event ? address.event.name : address.name,
          "provider":address.providedBy,
          "distance":address.distance,
          "type":address.type,
          "id":address.id,
          "regionCode":address.regionCode
        };
      return null;
    }




  }
};
