/**

    Copyright (c) 2018, MOBICOOP. All rights reserved.
    This project is dual licensed under AGPL and proprietary licence.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see gnu.org/licenses.

    Licence MOBICOOP described in the file
    LICENSE
**************************/
import { toastController } from '@ionic/vue';
import {isPlatform} from "@ionic/core";

export const toast = {
  methods: {
      async presentToast(text, color, position) {
        const toast = await toastController.create({
            message: text,
            duration: 4000,
            showCloseButton: false,
            position: position || 'top',
            color: color
        });
      await toast.present();
    },
    async locationErrorToast() {
      const buttons = [];
      if (isPlatform(window.document.defaultView, "capacitor")) {
        buttons.push({
          text: 'Réglages',
          role: 'info',
          handler: () => {
            if (isPlatform(window.document.defaultView, "ios")) {
              window.location.href = 'app-settings:';
            } else if (isPlatform(window.document.defaultView, "android")) {
              window.location.href = 'settings:location';
            }
          },
        })
      }


      buttons.push({
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Dismiss clicked');
        },
      })

      const toast = await toastController.create({
        message: this.$parent.$t("Commons.error-location"),
        showCloseButton: true,
        position: 'top',
        color: "danger",
        buttons: buttons
      });

      await toast.present();
    },
  }
};
