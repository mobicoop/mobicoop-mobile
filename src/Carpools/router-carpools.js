/**

 Copyright (c) 2018, MOBICOOP. All rights reserved.
 This project is dual licensed under AGPL and proprietary licence.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see gnu.org/licenses.

 Licence MOBICOOP described in the file
 LICENSE
 **************************/

// Global

import Home from './Home/Home.view.vue';
import MainTabComponent from './MainTabComponent/MainTabComponent.view.vue';
import Article from './Article/Article.view.vue';
import Login from './Login/Login.view.vue';
import Register from './Register/Register.view.vue';
import GeoSearch from './GeoSearch/GeoSearch.view.vue';
import Search from './Search/Search.view.vue';
import Messages from './Messages/Messages.view.vue';
import Communities from './Communities/Communities/Communities.view.vue';
import Profile from './Profile/Profile.view.vue';
import UpdateProfile from './Profile/UpdateProfile.view.vue';
import ProfileAlerts from './Profile/ProfileAlerts.view.vue';
import ProfilePrefs from './Profile/ProfilePrefs.view.vue';
import MyCarpools from './Profile/MyCarpools.view.vue';
import AcceptedCarpools from './Profile/AcceptedCarpools.view.vue';
import PostCarpool from './PostCarpool/PostCarpool.view.vue';
import Message from './Messages/Message.view.vue';
import PostCarpoolStep from './PostCarpool/PostCarpoolStep.view.vue';
import DetailCarpool from './DetailCarpool/DetailCarpool.view.vue';
import AskCarpool from './AskCarpool/AskCarpool.view.vue';
import ConfirmRegistration from './Register/ConfirmRegistration.view.vue';
import Community from './Communities/Community/Community.view.vue';
import Events from './Events/Events/Events.view.vue';
import Event from './Events/Event/Event.view.vue';
import Contact from './Contact/Contact.view.vue';
import Solidarity from './Solidarity/Solidarity.view.vue';
import PostCommunity from './Communities/PostCommunity/PostCommunity.view.vue';
import PostEvent from './Events/PostEvent/PostEvent.view.vue';
import Dynamic from './Dynamic/Dynamic.view.vue';
import PublicTransport from './PublicTransport/PublicTransport.view.vue';
import Reviews from "./Profile/Reviews";
import Payment from './Payment/Payment.view.vue';
import BankAccount from './Payment/BankAccount.view.vue';
import Paye from './Payment/Paye.view.vue';
import ResetPassword from './Login/ResetPassword.view.vue';
import ProfilePublic from './Profile/ProfilePublic.view.vue';
import WinBadge from './Gamification/WinBadge.vue';
import Badges from './Gamification/Badges.vue';
import RelayPoints from './RelayPoints/RelayPoints.view.vue';
import Prime from './Prime/Prime.view.vue';
import store from '../Shared/Store/store';
import Request from "../SolidaryTransport/Request/Request.view";
import RequestCheck from "../SolidaryTransport/Request/RequestCheck.view";
import RequestPath from "../SolidaryTransport/Request/RequestPath.view";
import RequestPunctual from "../SolidaryTransport/Request/RequestPunctual.view";
import RequestRegular from "../SolidaryTransport/Request/RequestRegular.view";
import RequestUser from "../SolidaryTransport/Request/RequestUser.view";
import RequestSummary from "../SolidaryTransport/Request/RequestSummary.view";
import EecIncentiveView from "./Prime/EecIncentive.view";
import InfoPermis from "./Prime/InfoPermis.view"
import MobHelp from "./Prime/Mob-Help.view";
import DetailRdex from "./DetailCarpool/DetailRdex.view.vue";
import SelectCommunity from "./Communities/SelectCommunity/SelectCommunity.view.vue";
import IncentivesView from "./Prime/Incentives.view.vue";
import IncentiveView from "./Prime/Incentive.view.vue";
import AutorisationParentaleView from "./Profile/AutorisationParentale.view.vue";


function guardAccesByLogin(to, from, next) {
  if (to.name !== 'login' && !store.getters.userId && !store.getters.userId) {
    store.commit('redirectionUrl_change', {name: to.name, params: to.params, query: to.query});
    next({ name: 'login'})
  } else {
    next()
  }
}

let preventAccessRequest = function (to, from, next) {
  let request = store.state.solidaryTransportStore.temporary.request;
  if (request.homeAddress) {
    next()
  } else {
    next({name: from.name.includes('carpool') ? 'carpoolsHome' : 'solidaryTransport.home'})
  }
}

function rdexRedirect(to, from, next) {
  if (!!to.params.id) {
    next({ name: 'search', query: {rdex: to.params.id}})
  } else {
    next({ name: 'carpoolsHome'})
  }
}

export default [{
    path: '',
    name: 'mainTabComponent',
    component: MainTabComponent,
    children: [
      {
        path: '',
        name: 'carpoolRedirect',
        redirect: { name: 'carpoolsHome' }
      },
    {
        path: 'home',
        name: 'carpoolsHome',
        component: Home,
    },
    {
        path: 'help',
        name: 'help',
        component: Article,
    },
    {
        path: 'login',
        name: 'login',
        component: Login
    },
    {
        path: 'register',
        name: 'register',
        component: Register,
    },
    {
        path: 'messages',
        name: 'messages',
        component: Messages,
    },
      {
        path: 'profile',
        name: 'profile',
        component: Profile,
      },
    {
        path: 'communities',
        name: 'communities',
        component: Communities,
        // beforeEnter: guardAccesByLogin
    },

    ]
},

{
    path: 'geosearch',
    name: 'geoSearch',
    component: GeoSearch
},

  {
    path: 'selectCommunity',
    name: 'selectCommunity',
    component: SelectCommunity
  },

{
    path: 'search',
    name: 'search',
    component: Search
  },
  {
    path: 'rdex/:id',
    name: 'rdex',
    beforeEnter: rdexRedirect
  },
  {
    path: 'update-profile',
    name: 'update-profile',
    component: UpdateProfile
},
{
    path: 'profile-alerts',
    name: 'profile-alerts',
    component: ProfileAlerts
},
{
    path: 'profile-prefs',
    name: 'profile-prefs',
    component: ProfilePrefs
},
{
    path: 'my-carpools',
    name: 'my-carpools',
    component: MyCarpools
},
{
    path: 'accepted-carpools',
    name: 'accepted-carpools',
    component: AcceptedCarpools
},
{
    path: 'reviews',
    name: 'reviews',
    component: Reviews
},
{
    path: 'payment',
    name: 'payment',
    component: Payment,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'payment/paye',
    name: 'paye',
    component: Paye,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'bank-account',
    name: 'bank-account',
    component: BankAccount,
},
  {
    path: 'eec-incentive',
    name: 'prime',
    component: Prime,
    beforeEnter: guardAccesByLogin
  },
  {
    path: 'mob-help',
    name: 'mob-help',
    component: MobHelp
  },
  {
    path: 'info-permis',
    name: 'info-permis',
    component: InfoPermis,
  },
{
    path: 'post-carpool-step',
    name: 'post-carpool-step',
    component: PostCarpoolStep
},
{
    path: 'post-carpool',
    name: 'post-carpool',
    component: PostCarpool,
    props: (route) => ({
        ...route.params
    }),
    beforeEnter: guardAccesByLogin
},
{
    path: 'message/:idRecipient/:idAsk',
    name: 'message',
    component: Message,
    /*
    props: (route) => ({
      ...route.params
    })
     */
},
{
    path: '/article/:id',
    name: 'article',
    component: Article,
},
{
    path: '/confirm-registration/:email',
    name: 'confirm-registration',
    component: ConfirmRegistration,
},
{
    path: '/carpool-detail/:param',
    name: 'carpool-detail',
    component: DetailCarpool,
    beforeEnter: guardAccesByLogin
},
  {
    path: '/rdex-detail/:param',
    name: 'rdex-detail',
    component: DetailRdex,
    beforeEnter: guardAccesByLogin
  },
{
    path: '/public-transport/:param',
    name: 'public-transport',
    component: PublicTransport,
},
{
    path: '/ask-carpool/:param',
    name: 'ask-carpool',
    component: AskCarpool,
    beforeEnter: guardAccesByLogin
},
{
    path: 'allcommunities',
    name: 'allcommunities',
    component: Communities,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'community/:id',
    name: 'carpool-community',
    component: Community,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'post-community',
    name: 'post-community',
    component: PostCommunity,
    beforeEnter: guardAccesByLogin
},
{
    path: 'events',
    name: 'carpool-events',
    component: Events,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'event/:id',
    name: 'carpool-event',
    component: Event,
    // beforeEnter: guardAccesByLogin
},
{
    path: 'post-event',
    name: 'post-event',
    component: PostEvent,
    beforeEnter: guardAccesByLogin
},
  {
    path: 'relaypoints',
    name: 'carpool-relaypoints',
    component: RelayPoints,
    // beforeEnter: guardAccesByLogin
  },
{
    path: 'contact',
    name: 'carpool-contact',
    component: Contact,
},
{
    path: 'solidarity',
    name: 'carpool-solidarity',
    component: Solidarity,
},
{
    path: 'dynamic',
    name: 'dynamic',
    component: Dynamic,
    beforeEnter: guardAccesByLogin
},
{
    path: 'reset-password',
    name: 'reset-password',
    component: ResetPassword,
},
{
    path: 'public/profile/:id',
    name: 'profile-public',
    component: ProfilePublic,
},
{
    path: 'win-badge',
    name: 'win-badge',
    component: WinBadge
},
{
    path: 'my-badges',
    name: 'my-badges',
    component: Badges
},
  {
    path: 'user/sso/eec-incentive',
    name: 'eec-incentive',
    component: EecIncentiveView
  },
  {
    path: 'user/sso/login',
    name: 'eec-login',
    component: EecIncentiveView
  },
  {
    path: 'incentives',
    name: 'incentives',
    component: IncentivesView
  },
  {
    path: 'incentive',
    name: 'incentive',
    component: IncentiveView
  },
  {
    path: 'autorisation-parentale/:id',
    name: 'autorisation-parentale',
    component: AutorisationParentaleView
  },
  // REQUEST ----- ( start here )
  {
    path: 'solidary/home/request',
    name: 'carpool.solidary.home.request',
    component: Request,
    meta: { type: 'request' }
  },
  {
    path: 'solidary/home/request/check',
    name: 'carpool.solidary.home.request.check',
    component: RequestCheck,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  {
    path: 'solidary/home/request/path',
    name: 'carpool.solidary.home.request.path',
    component: RequestPath,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  {
    path: 'solidary/home/request/punctual',
    name: 'carpool.solidary.home.request.punctual',
    component: RequestPunctual,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  {
    path: 'solidary/home/request/regular',
    name: 'carpool.solidary.home.request.regular',
    component: RequestRegular,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  {
    path: 'solidary/home/request/user',
    name: 'carpool.solidary.home.request.user',
    component: RequestUser,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  {
    path: 'solidary/home/request/summary',
    name: 'carpool.solidary.home.request.summary',
    component: RequestSummary,
    meta: { type: 'request' },
    beforeEnter: preventAccessRequest
  },
  // REQUEST ----- ( end here )
]
